# Construct and Divide

A barebones Tower Defence prototype.

Version 0.1 includes features such as:

- Graphics
- A single map
- A single tower
- A single enemy
- 5 identical waves
- Economy
- No win state
- Camera movement
- A auto-skipped main menu

Plans for a ton of additional features, however, just a hobby, won't be big and professional like $td.

This repo also serves as a testing platform for git/gitlab tools and features.
