# Credits

## Fonts

- [Liberator](http://www.losttype.com/font/?name=liberator) by [Lost Type](http://www.losttype.com/) - [Personal use license](http://www.losttype.com/images/samples/License_Personal_Sample.pdf)
- [Airplane](http://www.losttype.com/font/?name=airplane) by [Lost Type](http://www.losttype.com/) - [Personal use license](http://www.losttype.com/images/samples/License_Personal_Sample.pdf)

