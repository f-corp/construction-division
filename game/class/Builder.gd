class_name Builder, "res://asset/image/class_builder.svg"
extends Spatial
# Game uses Builder to add Buildings to the map
# A "marker" is created when the build mode is started
# The marker looks like the selected Bulding and
# uses the Building collision to determine if the player
# can build at the clicked position


signal refresh_buttons(resources)


var active := false
var areas_blocking_building := 0
var align_to_grid := false
var resources := 25 setget set_resources
var selected_building: PackedScene
var map_marker := Spatial.new()
var buildings := Spatial.new()
var label: Label


func init(resources_label: Label, parent: Node = owner) -> void:
	label = resources_label
	label.text = resources as String
	parent.add_child(buildings)
	parent.add_child(map_marker)
	var error = connect("refresh_buttons", parent, "refresh_buttons")
	assert(not error, "connect(refresh_buttons) failed (%d)" % error)

func toggle(building: PackedScene) -> void:
	if started():
		stop()
	elif stopped():
		start(building)


func start(building: PackedScene) -> void:
	active = true
	selected_building = building
	var marker = building.instance()
	var a = marker.get_node("Area")
	map_marker.add_child(marker)

	var error
	error = a.connect("area_entered", self, "block_marker")
	assert(not error, "connect(block_marker) failed (%d)" % error)
	error = a.connect("area_exited", self, "unblock_marker")
	assert(not error, "connect(unblock_marker) failed (%d)" % error)


func stop() -> void:
	active = false
	reset_marker()


func started() -> bool:
	return active


func stopped() -> bool:
	return not active


func select_building(building: PackedScene) -> void:
	selected_building = building


func place_building(v3: Vector3) -> void:
	var b = selected_building.instance()
	buildings.add_child(b)
	if align_to_grid:
		v3 = align(v3)
	b.init(v3)
	self.resources -= b.cost
	stop()


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("align_to_grid"):
		set_align(true)
	elif event.is_action_released("align_to_grid"):
		set_align(false)
	elif event.is_action_released("ui_cancel"):
		stop()


func align(v3: Vector3) -> Vector3:
	return Vector3(round(v3.x), 0, round(v3.z))


func set_align(value: bool) -> void:
	align_to_grid = value


func is_aligned() -> bool:
	return align_to_grid


func block_marker(_area: Area = null) -> void:
	areas_blocking_building += 1


func unblock_marker(_area: Area = null) -> void:
	areas_blocking_building -= 1


func is_blocked() -> int:
	return areas_blocking_building


func not_blocked() -> bool:
	return not areas_blocking_building


func move_marker(v3: Vector3) -> void:
	if align_to_grid:
		v3 = align(v3)
	map_marker.transform.origin.x = v3.x
	map_marker.transform.origin.z = v3.z


func reset_marker() -> void:
	for child in map_marker.get_children():
		child.free()


func get_resources() -> int:
	return resources


func add_resources(value: int) -> void:
	self.resources += value


func set_resources(value: int) -> void:
	resources = value
	label.text = resources as String
	emit_signal("refresh_buttons", resources)


func _on_area_tower_input(_0, e, m: Vector3, _2, _3) -> void:
	if started() and _is_left_click(e) and not_blocked():
		place_building(m)
	elif e is InputEventMouseMotion and started():
		move_marker(m)


func _is_left_click(event: InputEvent) -> bool:
	var is_left_click = false
	if event is InputEventMouseButton and \
		event.get_button_index() == 1 and \
		event.pressed:
			is_left_click = true
	return is_left_click
