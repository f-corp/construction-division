class_name Building, "res://asset/image/class_building.svg"
extends Spatial
# Core logic for tower and other builings
# Handles attacks, cooldown, damage etc
# Created nodes based on inspector values
# - Timer for cooldown
# - Area for collision (range) with Enemy
# root node will warn, since it requires a PathFollow as parent


export var cooldown := 5.5
export var damage := 0 # french for damage
export var building_name := "A building has no name"
export var attack_range := 7
export var node_looking_at_enemies: NodePath
export var cost: int


var enemies: Dictionary
var active := false
var placed := false
var eyes


func init(position: Vector3) -> void:
	placed = true
	active = true
	$cooldown.stop()
	set_position(position)
	eyes = get_node_or_null(node_looking_at_enemies)


func _ready() -> void:
	add_fallback_nodes()


func _process(_delta: float) -> void:
	if enemies and placed:
		var enemy = get_closest()
		if eyes:
			eyes.look_at(enemy.global_transform.origin, Vector3.UP)


func attack(area: Area) -> void:
	area.get_parent().take_damage(damage)
	$cooldown.start()
	active = false


func get_closest() -> Enemy:
	var closest = { offset = 0 }
	for enemy in enemies.values():
		if enemy.offset > closest.offset:
			closest = enemy
	return closest


func set_position(v3: Vector3) -> void:
	self.transform.origin.x = v3.x
	self.transform.origin.y = 0
	self.transform.origin.z = v3.z


func _on_cooldown_timeout() -> void:
	active = true


func _on_in_range(area: Area) -> void:
	var enemy = area.get_parent()
	enemies[area] = enemy

# warning-ignore:return_value_discarded
func _on_out_of_range(area: Area) -> void:
	enemies.erase(area)


func add_fallback_nodes() -> void:
	if get_node_or_null("range") == null:
		var a = Area.new()
		var c = CollisionShape.new()
		a.name = "range"
		a.set_collision_mask_bit(3, true)
		a.set_collision_mask_bit(0, false)
		a.set_collision_layer_bit(0, false)
		a.connect("area_entered", self, "_on_in_range")
		a.connect("area_exited", self, "_on_out_of_range")
		c.shape = SphereShape.new()
		c.shape.radius = attack_range
		a.add_child(c)
		add_child(a)

	if get_node_or_null("cooldown") == null:
		var timer = Timer.new()
		timer.wait_time = cooldown
		timer.name = "cooldown"
		timer.connect("timeout", self, "_on_cooldown_timeout")
		add_child(timer)
