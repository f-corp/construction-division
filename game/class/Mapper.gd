class_name Mapper, "res://asset/image/class_mapper.svg"
extends Node
# Connects a map scene to Main and Builder


var instance: Spatial


func init(id: String, builder: Builder):
	instance = load("res://map/%s/map.tscn" % id).instance()
	owner.add_child(instance)
	owner.map = instance
	
	# Builder needs to know about map collisions
	for area in get_tree().get_nodes_in_group("area_tower"):
		area.connect("input_event", builder, "_on_area_tower_input")
