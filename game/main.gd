extends Spatial


var version := "v0.1"
var lives := 10
var map := Spatial


onready var builder := $builder
onready var spawner := $spawner
onready var mapper := $mapper
onready var gui = {
	resources = $camera/top/grid/resources/value,
	lives = $camera/top/grid/lives/value,
	waves = $camera/top/grid/waves/value,
	status = $camera/bottom/grid/status,
	buildings = get_tree().get_nodes_in_group("button_building"),
}


func _ready() -> void:
	builder.init(gui.resources)
	mapper.init("my-awesome-map", builder)
	spawner.init("res://map/waves.json", builder, gui.waves)
	spawner.start_wave(0)

	gui.status.text = "Construct and Divide %s" % version
	gui.lives.text = lives as String
	gui.resources.text = builder.get_resources() as String


func  refresh_buttons(resources: int) -> void:
	for btn in gui.buildings:
		btn.check_can_afford(resources)


func _input(event: InputEvent) -> void:
	if event.is_action_released("quit"):
		get_tree().quit()


func _on_enemy_despawned() -> void:
	lives -= 1
	gui.lives.text = lives as String
