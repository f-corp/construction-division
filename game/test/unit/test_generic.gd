extends "res://addons/gut/test.gd"

var buildings = []
var Main = load("res://main.gd")
var _build

func before_all():
	gut.p("Add all buildings to array")
	var building_names = ["tower"]
	var buildings_path = "res://building/%s.tscn"
	for name in building_names:
		var b = { name = name }
		b.obj = load(buildings_path % name)
		b.inst = b.obj.instance()
		b.col = b.inst.get_node("Area/CollisionShape")
		buildings.append(b)

func before_each():
	_build = Builder.new()
	_build.init(Main.new())

func test_tower_have_collision_shapes_in_correct_path():
	for b in buildings:
		gut.p(b.name)
		assert_not_null(b.col.shape, "building %s missing collision" % b.name)

func test_main_scene_created():
	assert_is(Main.new(), Spatial, "New main scene should be Spatial")

func test_build_init():
	assert_is(_build.buildings, Spatial, "Buildings Spatial missing")
	assert_is(_build.map_marker, Spatial, "Marker Spatial missing")

func test_build_enable_disable():
	assert_false(_build.started(), "build should init as disabled")
	_build.start(buildings[0].obj)
	assert_true(_build.started(), "build should be enabled()")
	_build.stop()
	assert_true(_build.stopped(), "build should be disabled()")

func test_build_align():
	assert_false(_build.is_aligned(), "build should init 'align' as disabled")
	_build.set_align(true)
	assert_true(_build.is_aligned(), "build align should be True")

func test_build_block():
	_build.block_marker()
	assert_eq(_build.is_blocked(), 1, "Areas blocking the marker should be 1")
	_build.unblock_marker()
	assert_true(_build.not_blocked(), "No areas should block the marker")

func test_build_start():
	for b in buildings:
		gut.p(b.name)
		_build.start(b.obj)
		assert_is(_build.map_marker.get_child(0), Spatial, "building %s should be child of marker" % b.name)

func test_build_marker_connections():
	for b in buildings:
		gut.p(b.name)
		_build.start(b.obj)
		var area = _build.map_marker.get_child(0).get_node("Area")
		assert_connected(area, _build, "area_entered", "block_marker")
		assert_connected(area, _build, "area_exited", "unblock_marker")

func test_build_signals_emitted():
	for b in buildings:
		gut.p(b.name)
		_build.start(b.obj)
		var area = _build.map_marker.get_child(0).get_node("Area")
		watch_signals(area)
		area.emit_signal("area_exited")
		assert_signal_emitted(area, 'area_exited', "building %s should signal main scene" % b.name)
