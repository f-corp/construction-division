extends Building


onready var laser := $top/laser


func _ready() -> void:
	laser.translation = Vector3(0, 0, 0)
	laser.visible = false
	var error = $top/laser/Area.connect("area_entered", self, "attack")
	assert(not error, "connect(_on_hit) failed (%d)" % error)


func _process(delta: float) -> void:
	if enemies and active:
		laser.visible = true
		laser.translation.z -= delta * 40
	else:
		laser.visible = false
		laser.translation.z = 0
